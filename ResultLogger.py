#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 17:04:05 2019

@author: leandro
"""

import os
import datetime
import Metrics

class ResultLogger:
    
    fields = {
        "date" : "",
        "dataset" : "",
        "notes" : ""
    }
    logFilePath = ""
    
    def __init__(self, logFilePath):
        self.logFilePath = logFilePath
    
    def addField(self, f):
        self.fields[f] = ""
    def addFields(self, fields):
        for f in fields:
            self.addField(f)
    def getFields(self):
        strLine = ""
        for f in self.fields:
            strLine += "%s," % f
        return strLine[0:-1] + "\n"
    def setValue(self, f, value):
#        if not f in self.fields:
#            raise ValueError("Field not found")
#        else:
        if type(value) is list:
            value = str(value).replace(",",";")

        self.fields[f] = value
    def getValues(self):
        strLine = ""
        for f in self.fields:
            strLine += "%s," % str(self.fields[f])
        return strLine[0:-1] + "\n"
    def resetValues(self):
        for f in self.fields:
            self.fields[f] = ""
    
    def saveMetrics(self, metrics, averageMetrics, currentEpoch):
        #para cada uno
        for seed, seedMetrics in enumerate(metrics):            
            for t in ["test", "train"]:
                for m in Metrics.metricsUsed:
                    self.setValue("%s%s-%d" % (m,t.capitalize(),seed+1), seedMetrics[currentEpoch][t][m])
        
        #para los average
        for t in ["test", "train"]:
            for m in averageMetrics[t][currentEpoch]:
                self.setValue("%s%s" % (m, t.capitalize()), averageMetrics[t][currentEpoch][m])
    
    # a esto habría que agregarle database_name por las dudas
    def isRunAlreadyMade(self, variables):
        if not os.path.isfile(self.logFilePath):
            return False
        
        header = None
        
        with open(self.logFilePath, "r") as f:
            for row in f:
                arrRow = row.split(",") 
                arrRow[-1] = arrRow[-1][0:-1] #el \n del final
                
                if header is None:
                   header = arrRow
                   continue
                
                rowFound = True
                for v in variables:
                    runValue = variables[v]
                    csvValue = arrRow[ header.index(v) ]
    
                    if type(runValue) is float:
                        csvValue = float(csvValue)
                    else:
                        runValue = str(runValue).replace(",",";")
                        csvValue = str(csvValue)
    
                    if runValue != csvValue:
                        rowFound = False
                        break
                
                if rowFound:
                    return True
            
        
        return False
    
    def log(self):
        if not os.path.isfile(self.logFilePath):
            with open(self.logFilePath, 'w') as logFile:
                logFile.write( self.getFields() )
        
        with open(self.logFilePath, 'a') as logFile:
            self.fields["date"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            logFile.write( self.getValues() )
        
        self.resetValues()
    
