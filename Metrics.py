#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 16:02:57 2019

@author: leandro
"""

from sklearn.metrics import roc_curve, roc_auc_score
import numpy as np

metricsUsed = ["eer", "auc"]

def getMetricsPred(y_train, y_train_pred, y_test = None, y_test_pred = None, time = 0, withThreshold = False):
    metrics = {
        "train" : {},
        "time" : time
    }
    
    if y_test is not None:
        metrics["test"] = {}
        metrics["test"]["eer"] = _eer(y_test, y_test_pred, withThreshold)
        metrics["test"]["auc"] = roc_auc_score(y_test, y_test_pred)

    metrics["train"]["eer"] = _eer(y_train, y_train_pred, withThreshold)
    metrics["train"]["auc"] = roc_auc_score(y_train, y_train_pred)
    
    return metrics

def getMetrics(model, X_train, y_train, X_test, y_test, time):
    metrics = {
        "train" : {},
        "test" : {},
        "time" : time
    }
     
   
    y_pred = model.predict(X_test) #esto tarda
    metrics["test"]["eer"] = _eer(y_test, y_pred)
    metrics["test"]["auc"] = roc_auc_score(y_test, y_pred)

    y_pred = model.predict(X_train) #esto tarda mas
    metrics["train"]["eer"] = _eer(y_train, y_pred)
    metrics["train"]["auc"] = roc_auc_score(y_train, y_pred)
    
    return metrics

def getAverageMetrics(metrics):
    averageMetrics = {
        "train" : [],
        "test" : [],
        "time" : []
    }
    
    qtySeeds = len(metrics)
    qtyEpochSteps = len(metrics[0])
    
    for i in range(qtyEpochSteps):
        averageMetrics["train"].append({})
        averageMetrics["test"].append({})
        for m in metricsUsed:
            averageMetrics["train"][i]["%sMean" % m] = 0
            averageMetrics["test"][i]["%sMean" % m] = 0
            averageMetrics["train"][i]["%sStdev" % m] = 0
            averageMetrics["test"][i]["%sStdev" % m] = 0
        averageMetrics["time"].append(0)
        
        #mean
        for j in range(qtySeeds):
            for m in metricsUsed:
                averageMetrics["train"][i]["%sMean" % m] += metrics[j][i]["train"][m]
                averageMetrics["test"][i]["%sMean" % m] += metrics[j][i]["test"][m]
            averageMetrics["time"][i] += metrics[j][i]["time"]
        
        for m in metricsUsed:
            averageMetrics["train"][i]["%sMean" % m] /= qtySeeds
            averageMetrics["test"][i]["%sMean" % m] /= qtySeeds
        averageMetrics["time"][i] /= qtySeeds
    
        #stdev
        for j in range(qtySeeds):
            for m in metricsUsed:
                averageMetrics["train"][i]["%sStdev" % m] += (metrics[j][i]["train"][m] - averageMetrics["train"][i]["%sMean" % m]) ** 2
                averageMetrics["test"][i]["%sStdev" % m] += (metrics[j][i]["test"][m] - averageMetrics["test"][i]["%sMean" % m]) ** 2
        for m in metricsUsed:
            averageMetrics["train"][i]["%sStdev" % m] /= qtySeeds - 1
            averageMetrics["test"][i]["%sStdev" % m] /= qtySeeds - 1

            trainDiv = averageMetrics["train"][i]["%sStdev" % m] ** (1/2)
            if trainDiv != 0:
                averageMetrics["train"][i]["%sStdev" % m] /= trainDiv

            testDiv = averageMetrics["test"][i]["%sStdev" % m] ** (1/2)
            if testDiv != 0:
                averageMetrics["test"][i]["%sStdev" % m] /= testDiv
	    
    return averageMetrics
        
def _eer(y, y_pred, withThreshold):
    # all fpr, tpr, fnr, fnr, threshold are lists (in the format of np.array)
    fpr, tpr, threshold = roc_curve(y, y_pred, pos_label=1)
    fnr = 1 - tpr

    # the threshold of fnr == fpr
    eer_threshold = threshold[np.nanargmin(np.absolute((fnr - fpr)))]

    # theoretically eer from fpr and eer from fnr should be identical but they can be slightly differ in reality
    eer_1 = fpr[np.nanargmin(np.absolute((fnr - fpr)))]
    eer_2 = fnr[np.nanargmin(np.absolute((fnr - fpr)))]

    # return the mean of eer from fpr and from fnr
    eer = (eer_1 + eer_2) / 2
    
    if not withThreshold:
        return eer
    else:
        return eer, eer_threshold
