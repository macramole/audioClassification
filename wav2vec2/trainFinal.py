from transformers import Wav2Vec2FeatureExtractor, Wav2Vec2Model
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

import os
import gc
import subprocess
import sys
sys.path.append('../')

import Datasets
import Metrics
import utils
from models import *

from enum import Enum
from tqdm import tqdm

class ModelName(Enum):
    Conv1D = "Conv1D",
    Dense = "Dense"

### Hyperparams
batch_size = 16
epochs = 15
pretrainedModel = "voxpopuli" #"xlsr" 
freeze = False
dropoutProb = 0.1
aug = False
tuneModelName = ModelName.Conv1D
optimName = "Adam"
warmup = False
lr = 0.00001
##############

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("Using device: ", device)

pretrainedModelURL = Wav2VecFineTune.PRETRAINED_VOXPOPULI if pretrainedModel == "voxpopuli" else Wav2VecFineTune.PRETRAINED_XLSR

print("Loading Dataset...")
datasetName, dfAudio, dfMetadata = Datasets.getQOMvsSPA( Datasets.DATASET_RAW )
print("Done")

## Go go go

print("Training...")
show_gpu("Inicial:")

trainset = Dataset(dfAudio, dfMetadata, wav2vec2ModelURL = pretrainedModelURL, augmentation=aug)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, 
                                        shuffle=True, num_workers=7,
                                        collate_fn=trainset.collate_wrapper)

if ( tuneModelName == ModelName.Dense ):
    tuneModel = Wav2VecFineTune( pretrainedModelURL, freeze=freeze, dropoutProb=dropoutProb, denseDimension=512 ).to(device)
else:
    tuneModel = Wav2VecFineTune_Conv1D( pretrainedModelURL, freeze=freeze, dropoutProb=dropoutProb ).to(device)

criterion = nn.BCELoss()

if optimName == "SGD":
    optimizer = optim.SGD(tuneModel.parameters(), lr=lr, momentum=0.9)
elif optimName == "Adam":
    if not warmup:
        optimizer = optim.Adam(tuneModel.parameters(), lr=lr)
    else:
        optimizer = NoamOpt(64, 7000,
                            torch.optim.Adam(tuneModel.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9))
elif optimName == "Adadelta":
    optimizer = optim.Adadelta(tuneModel.parameters())

#     optimizer = optim.Adam(tuneModel.parameters(), lr=0.001)
#     optimizer = NoamOpt(64, 7000,
#         torch.optim.Adam(tuneModel.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9))
#     optimizer = optim.Adadelta(tuneModel.parameters())

show_gpu("Antes de entrenar:")

tensorboardWriter = SummaryWriter(
    comment=f"---FINAL_{pretrainedModel}-total-batch_{batch_size}-Conv1D-{optimName}-WarmUp_{warmup}-Freeze_{freeze}-NObatchNorm-dropout_{dropoutProb}-aug_{aug}"
)

print("Now training...")

for epoch in tqdm(range(1,epochs+1)):
    ##########
    # TRAIN
    ##########
    
    train_epoch_loss = 0
    #para calcular AUC y EER
    train_outputs = []
    train_true_labels = []
    for i, data in enumerate(trainloader, 0):
        labels = data["labels"].to(device) 
        inputs = data["input_values"].to(device)
        masks = data["attention_mask"].to(device)

        if not warmup:
            optimizer.zero_grad()
        else:
            optimizer.optimizer.zero_grad() #warmup

        # forward + backward + optimize
        outputs = tuneModel(inputs, masks)      
        loss = criterion(outputs, labels)
        loss.backward()
        
        optimizer.step()
        if warmup:
            tensorboardWriter.add_scalar(f"lr/train/Final", optimizer.rate(), epoch) #warmup

        train_epoch_loss += loss.item()
        train_outputs.append(outputs.detach().cpu().numpy())
        train_true_labels.append(labels.detach().cpu().numpy())

    train_epoch_loss /= len(trainloader)
    tensorboardWriter.add_scalar(f"Loss/train/Final", train_epoch_loss, epoch)
    train_outputs = np.concatenate( train_outputs )
    train_true_labels = np.concatenate( train_true_labels )

    del inputs, outputs, loss

print("Saving model...")
torch.save(tuneModel.state_dict(), f"models/modelo-final-{epochs}.pt")
print("Done, have a good day ! :o)")