from transformers import Wav2Vec2FeatureExtractor, Wav2Vec2Model
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

import os
import gc
import subprocess
import sys
sys.path.append('../')

import Datasets
import Metrics
import utils
from models import *

from enum import Enum
from tqdm import tqdm

class ModelName(Enum):
    Conv1D = "Conv1D",
    Dense = "Dense"

### Hyperparams
batch_size = 64 #no hablé nada sobre esto !!!
epochs = 120
pretrainedModel = "voxpopuli" #"xlsr" 
freeze = True
dropoutProb = 0.1
denseDimension = 512 # no lo uso si "Conv1D"
split = "family" #"estratificado"
aug = False
tuneModelName = ModelName.Conv1D
optimName = "Adam"
warmup = False
lr = 0.0001
##############

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("Using device: ", device)

pretrainedModelURL = Wav2VecFineTune.PRETRAINED_VOXPOPULI if pretrainedModel == "voxpopuli" else Wav2VecFineTune.PRETRAINED_XLSR

print("Loading Dataset...")
datasetName, dfAudio, dfMetadata = Datasets.getQOMvsSPA( Datasets.DATASET_RAW, split=split )
kfolds = len(dfMetadata.kfold.unique())
print(f"{kfolds} kfolds")
print("Done")

if tuneModelName == ModelName.Conv1D:
    tensorboardWriter = SummaryWriter(
        comment=f"---{pretrainedModel}-{split}-batch_{batch_size}-{tuneModelName}-{optimName}-WarmUp_{warmup}-Freeze_{freeze}-dropout_{dropoutProb}-aug_{aug}-_-lr_{lr}"
    )
else:
    tensorboardWriter = SummaryWriter(
        comment=f"---{pretrainedModel}-{split}-batch_{batch_size}-[{denseDimension},{denseDimension}]-{optimName}-WarmUp_{warmup}-Freeze_{freeze}-NObatchNorm-dropout_{dropoutProb}-aug_{aug}"
    )

# hparams = {
#     "batch_size" : batch_size,
#     "model" : "voxpopuli",
#     "freeze" : freeze,
#     "dropout" : dropoutProb,
#     "dense" : f"[{denseDimension},{denseDimension}]",
#     "split" : split
# }
# tensorboardWriter.add_hparams(hparams,{})

## Go go go

print("Training...")
show_gpu("Inicial:")

for testFold in range(kfolds):
    
    print(f"Fold {testFold+1}/{kfolds}")
    
    trainset = Dataset(dfAudio, dfMetadata, kfolds, foldOut = testFold, wav2vec2ModelURL = pretrainedModelURL, augmentation=aug)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, 
                                            shuffle=True, num_workers=7,
                                            collate_fn=trainset.collate_wrapper)
    testset = Dataset(dfAudio, dfMetadata, kfolds, fold = testFold, wav2vec2ModelURL = pretrainedModelURL)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, 
                                            shuffle=False, num_workers=7,
                                            collate_fn=testset.collate_wrapper)
    
    if ( tuneModelName == ModelName.Dense ):
        tuneModel = Wav2VecFineTune( pretrainedModelURL, freeze=freeze, dropoutProb=dropoutProb, denseDimension=denseDimension ).to(device)
    else:
        tuneModel = Wav2VecFineTune_Conv1D( pretrainedModelURL, freeze=freeze, dropoutProb=dropoutProb ).to(device)
    
    criterion = nn.BCELoss()

    if optimName == "SGD":
        optimizer = optim.SGD(tuneModel.parameters(), lr=lr, momentum=0.9)
    elif optimName == "Adam":
        if not warmup:
            optimizer = optim.Adam(tuneModel.parameters(), lr=lr)
        else:
            optimizer = NoamOpt(64, 7000,
                                torch.optim.Adam(tuneModel.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9))
    elif optimName == "Adadelta":
        optimizer = optim.Adadelta(tuneModel.parameters())

#     optimizer = optim.Adam(tuneModel.parameters(), lr=0.001)
#     optimizer = NoamOpt(64, 7000,
#         torch.optim.Adam(tuneModel.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9))
#     optimizer = optim.Adadelta(tuneModel.parameters())
    
    show_gpu("Antes de entrenar:")
    
    for epoch in range(1,epochs+1):
        ##########
        # TRAIN
        ##########
        
        train_epoch_loss = 0
        #para calcular AUC y EER
        train_outputs = []
        train_true_labels = []
        for i, data in enumerate(trainloader, 0):
            labels = data["labels"].to(device) 
            inputs = data["input_values"].to(device)
            masks = data["attention_mask"].to(device)

            if not warmup:
                optimizer.zero_grad()
            else:
                optimizer.optimizer.zero_grad() #warmup

            # forward + backward + optimize
            outputs = tuneModel(inputs, masks)      
            loss = criterion(outputs, labels)
            loss.backward()
            
            optimizer.step()
            if warmup:
                tensorboardWriter.add_scalar(f"lr/train/{testFold}", optimizer.rate(), epoch) #warmup

            train_epoch_loss += loss.item()
            train_outputs.append(outputs.detach().cpu().numpy())
            train_true_labels.append(labels.detach().cpu().numpy())

        train_epoch_loss /= len(trainloader)
        tensorboardWriter.add_scalar(f"Loss/train/{testFold}", train_epoch_loss, epoch)
        train_outputs = np.concatenate( train_outputs )
        train_true_labels = np.concatenate( train_true_labels )

        del inputs, outputs, loss
        
        ##########
        # EVAL
        ##########
        
        with torch.no_grad():
            tuneModel.eval()
            test_epoch_loss = 0
            test_outputs = []
            test_true_labels = []
            for i, data in enumerate(testloader, 0):
                labels = data["labels"].to(device) 
                inputs = data["input_values"].to(device)
                masks = data["attention_mask"].to(device)

                outputs = tuneModel(inputs, masks)
                loss = criterion(outputs, labels)

                test_epoch_loss += loss.item()
                test_outputs.append(outputs.detach().cpu().numpy())
                test_true_labels.append(labels.detach().cpu().numpy())

            test_epoch_loss /= len(testloader)
            tensorboardWriter.add_scalar(f"Loss/test/{testFold}", test_epoch_loss, epoch)
            test_outputs = np.concatenate( test_outputs )
            test_true_labels = np.concatenate( test_true_labels )

            ##########
            # Metrics EER && AUC
            ##########
            
            y_train = train_true_labels
            y_train_pred = train_outputs
            y_test = test_true_labels
            y_test_pred = test_outputs
            time = 0

            metrics = Metrics.getMetricsPred(y_train, y_train_pred, y_test, y_test_pred, time, True)
            tensorboardWriter.add_scalar(f"EER/train/{testFold}", metrics["train"]["eer"][0], epoch)
            tensorboardWriter.add_scalar(f"EER_threshold/train/{testFold}", metrics["train"]["eer"][1], epoch)
            tensorboardWriter.add_scalar(f"AUC/train/{testFold}", metrics["train"]["auc"], epoch)
            tensorboardWriter.add_scalar(f"EER/test/{testFold}", metrics["test"]["eer"][0], epoch)
            tensorboardWriter.add_scalar(f"EER_threshold/test/{testFold}", metrics["test"]["eer"][1], epoch)
            tensorboardWriter.add_scalar(f"AUC/test/{testFold}", metrics["test"]["auc"], epoch)

            print(f'Epoch {epoch+0:02}: | Train Loss: {train_epoch_loss:.5f} | Val Loss: {test_epoch_loss:.5f}')
            
            del train_outputs, test_outputs, y_train, y_train_pred, y_test, y_test_pred, metrics
            del train_true_labels, test_true_labels
            show_gpu("Mem actual:")
    
    # liberar espacio entre folds
    show_gpu("Fin del fold, antes de liberar:")
    del inputs, outputs, tuneModel, trainset, testset, trainloader, testloader, criterion, optimizer
    gc.collect()
    torch.cuda.empty_cache()
    show_gpu("Fin del fold, después de liberar:")

tensorboardWriter.flush()
tensorboardWriter.close()
