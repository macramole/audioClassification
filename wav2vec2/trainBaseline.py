import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

import gc
import sys
sys.path.append('../')

import Datasets
import Metrics
from models import *

from enum import Enum
from tqdm import tqdm

### Hyperparams
batch_size = 64
epochs = 300
dropoutProb = 0.1
split = "family" #"estratificado"
aug = False
optimName = "Adam"
warmup = False
lr = 0.0001
##############

USE_TENSORBOARD = True

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("Using device: ", device)

print("Loading Dataset...")
datasetName, dfAudio, dfMetadata = Datasets.getQOMvsSPA( Datasets.DATASET_RAW, split=split )
kfolds = len(dfMetadata.kfold.unique())
print(f"{kfolds} kfolds")
print("Done")

for lr in [ 0.001, 0.01, 0.00001, 0.000001  ]:

    if USE_TENSORBOARD:
        tensorboardWriter = SummaryWriter(
            comment=f"---baseline-{split}-batch_{batch_size}-Conv1D-{optimName}-WarmUp_{warmup}-Freeze_False-dropout_{dropoutProb}-aug_{aug}-_-lr_{lr}"
        )
    else:
        class TensorboardWriterFoo(object):
            def add_scalar(self, *args):
                pass
            def flush(self, *args):
                pass
            def close(self, *args):
                pass
        
        tensorboardWriter = TensorboardWriterFoo()

    ## Go go go

    print("Training...")
    show_gpu("Inicial:")

    for testFold in range(kfolds):
        
        print(f"Fold {testFold+1}/{kfolds}")
        
        trainset = DatasetBaseline(dfAudio, dfMetadata, kfolds, foldOut = testFold, augmentation=aug)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, 
                                                shuffle=True, num_workers=7,
                                                collate_fn=trainset.collate_wrapper)
        testset = DatasetBaseline(dfAudio, dfMetadata, kfolds, fold = testFold)
        testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, 
                                                shuffle=False, num_workers=7,
                                                collate_fn=testset.collate_wrapper)
        
        model = Baseline( dropoutProb=dropoutProb ).to(device)

        criterion = nn.BCELoss()

        if optimName == "SGD":
            optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)
        elif optimName == "Adam":
            if not warmup:
                optimizer = optim.Adam(model.parameters(), lr=lr)
            else:
                optimizer = NoamOpt(64, 7000,
                                    torch.optim.Adam(model.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9))
        elif optimName == "Adadelta":
            optimizer = optim.Adadelta(model.parameters())

        show_gpu("Antes de entrenar:")
        
        for epoch in range(1,epochs+1):
            ##########
            # TRAIN
            ##########
            
            train_epoch_loss = 0
            #para calcular AUC y EER
            train_outputs = []
            train_true_labels = []
            for i, data in enumerate(trainloader, 0):
                labels = data["labels"].to(device) 
                inputs = data["input_values"].to(device)

                if not warmup:
                    optimizer.zero_grad()
                else:
                    optimizer.optimizer.zero_grad() #warmup

                # forward + backward + optimize
                outputs = model(inputs)      
                loss = criterion(outputs, labels)
                loss.backward()
                
                optimizer.step()
                if warmup:
                    tensorboardWriter.add_scalar(f"lr/train/{testFold}", optimizer.rate(), epoch) #warmup

                train_epoch_loss += loss.item()
                train_outputs.append(outputs.detach().cpu().numpy())
                train_true_labels.append(labels.detach().cpu().numpy())

            train_epoch_loss /= len(trainloader)
            tensorboardWriter.add_scalar(f"Loss/train/{testFold}", train_epoch_loss, epoch)
            train_outputs = np.concatenate( train_outputs )
            train_true_labels = np.concatenate( train_true_labels )

            del inputs, outputs, loss
            
            ##########
            # EVAL
            ##########
            
            with torch.no_grad():
                model.eval()
                test_epoch_loss = 0
                test_outputs = []
                test_true_labels = []
                for i, data in enumerate(testloader, 0):
                    labels = data["labels"].to(device) 
                    inputs = data["input_values"].to(device)

                    outputs = model(inputs)
                    loss = criterion(outputs, labels)

                    test_epoch_loss += loss.item()
                    test_outputs.append(outputs.detach().cpu().numpy())
                    test_true_labels.append(labels.detach().cpu().numpy())

                test_epoch_loss /= len(testloader)
                tensorboardWriter.add_scalar(f"Loss/test/{testFold}", test_epoch_loss, epoch)
                test_outputs = np.concatenate( test_outputs )
                test_true_labels = np.concatenate( test_true_labels )

                ##########
                # Metrics EER && AUC
                ##########
                
                y_train = train_true_labels
                y_train_pred = train_outputs
                y_test = test_true_labels
                y_test_pred = test_outputs
                time = 0

                metrics = Metrics.getMetricsPred(y_train, y_train_pred, y_test, y_test_pred, time)
                tensorboardWriter.add_scalar(f"EER/train/{testFold}", metrics["train"]["eer"], epoch)
                tensorboardWriter.add_scalar(f"AUC/train/{testFold}", metrics["train"]["auc"], epoch)
                tensorboardWriter.add_scalar(f"EER/test/{testFold}", metrics["test"]["eer"], epoch)
                tensorboardWriter.add_scalar(f"AUC/test/{testFold}", metrics["test"]["auc"], epoch)

                print(f'Epoch {epoch+0:02}: | Train Loss: {train_epoch_loss:.5f} | Val Loss: {test_epoch_loss:.5f}')
                
                del train_outputs, test_outputs, y_train, y_train_pred, y_test, y_test_pred, metrics
                del train_true_labels, test_true_labels
                show_gpu("Mem actual:")
        
        # liberar espacio entre folds
        show_gpu("Fin del fold, antes de liberar:")
        del inputs, outputs, model, trainset, testset, trainloader, testloader, criterion, optimizer
        gc.collect()
        torch.cuda.empty_cache()
        show_gpu("Fin del fold, después de liberar:")

    tensorboardWriter.flush()
    tensorboardWriter.close()
