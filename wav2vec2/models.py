from curses import raw
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from transformers import Wav2Vec2FeatureExtractor, Wav2Vec2Model

import nlpaug.augmenter.audio as naa

import Datasets
import utils
from tqdm import tqdm

import librosa

DEBUG_MEMORY = False

class Baseline(nn.Module):
    
    def __init__(self, dropoutProb = 0.5):
        super().__init__()
        
        mfcc_with_deltas = 60
        layer1_outputs = 32#64
        layer2_outputs = 16#32

        self.layer1 = nn.Sequential(
            nn.Conv1d( in_channels = mfcc_with_deltas, out_channels = layer1_outputs, kernel_size=(1,) ),
            nn.BatchNorm1d( num_features=layer1_outputs ),
            nn.ReLU(),
            nn.Dropout( dropoutProb )
                      
        )
        self.layer2 = nn.Sequential(
            nn.Conv1d( in_channels = layer1_outputs, out_channels = layer2_outputs, kernel_size=(3, ) ),
            nn.BatchNorm1d( num_features=layer2_outputs ),
            nn.ReLU(),
            nn.Dropout( dropoutProb )
        )
        
        self.denseFinal = nn.Linear(in_features = layer2_outputs, out_features = 1)
    
    def forward(self, x):
        x = self.layer1(x)
        x = torch.squeeze(x, dim=-1)
        x = self.layer2(x)
        x = torch.mean(x,-1) #Pooling
        return torch.sigmoid( self.denseFinal(x) ) #entre 0 y 1


class Wav2VecFineTune_Conv1D(nn.Module):
    
    PRETRAINED_VOXPOPULI = "facebook/wav2vec2-large-100k-voxpopuli"
    PRETRAINED_XLSR = "facebook/wav2vec2-large-xlsr-53"
    PRETRAINED_XLSR_SPA = "facebook/wav2vec2-large-xlsr-53-spanish"
    
    def __init__(self, wav2vec2ModelURL, freeze = True, dropoutProb = 0.5):
        super().__init__()
        
        self.wav2vec2ModelURL = wav2vec2ModelURL
        
        #         torch.Size([10, 149, 1024]) BatchSize, frames(tiempo), features
        lastHiddenDimension = 149 * 1024  # 3 secs
#         lastHiddenDimension = 199 * 1024 # 4 secs
        
        self.w2v = Wav2Vec2Model.from_pretrained(wav2vec2ModelURL)
        for name, p in self.w2v.named_parameters():
            if freeze:
                p.requires_grad = False
            else :
                if "encoder" in name:
                    p.requires_grad = True
                else :
                    p.requires_grad = False
        
        layer1_outputs = 64
        layer2_outputs = 32

        self.layer1 = nn.Sequential(
            nn.Conv1d( in_channels = 1024, out_channels = layer1_outputs, kernel_size=(1,) ),
            nn.BatchNorm1d( num_features=layer1_outputs ),
            nn.ReLU(),
            nn.Dropout( dropoutProb )
                      
        )
        self.layer2 = nn.Sequential(
            nn.Conv1d( in_channels = layer1_outputs, out_channels = layer2_outputs, kernel_size=(3, ) ),
            nn.BatchNorm1d( num_features=layer2_outputs ),
            nn.ReLU(),
            nn.Dropout( dropoutProb )
        )
        
        self.denseFinal = nn.Linear(in_features = layer2_outputs, out_features = 1)
    
    def forward(self, x, attention_mask):
        x = self.w2v.forward(x, attention_mask).last_hidden_state
        x = x.swapaxes(1,2)
        x = self.layer1(x)
        x = torch.squeeze(x, dim=-1)
        x = self.layer2(x)
        x = torch.mean(x,-1) #Pooling
        return torch.sigmoid( self.denseFinal(x) ) #entre 0 y 1
    
class Wav2VecFineTune(nn.Module):
    
    PRETRAINED_VOXPOPULI = "facebook/wav2vec2-large-100k-voxpopuli"
    PRETRAINED_VOXPOPULI_ES = "facebook/wav2vec2-large-es-voxpopuli"
    PRETRAINED_VOXPOPULI_BASE_ES = "facebook/wav2vec2-base-es-voxpopuli-v2"
    PRETRAINED_XLSR = "facebook/wav2vec2-large-xlsr-53"
    PRETRAINED_XLSR_SPA = "facebook/wav2vec2-large-xlsr-53-spanish"
    
    def __init__(self, wav2vec2ModelURL, freeze = True, denseDimension = 512, dropoutProb = 0.5):
        super().__init__()
        
        self.wav2vec2ModelURL = wav2vec2ModelURL
        
#         torch.Size([10, 149, 1024]) BatchSize, frames(tiempo), features
        lastHiddenDimension = 149 * 1024  # 3 secs
#         lastHiddenDimension = 199 * 1024 # 4 secs
        
        self.w2v = Wav2Vec2Model.from_pretrained(wav2vec2ModelURL)
        for name, p in self.w2v.named_parameters():
            if freeze:
                p.requires_grad = False
            else :
                if "encoder" in name:
                    p.requires_grad = True
                else :
                    p.requires_grad = False
        
        self.flatten = nn.Flatten()
        
        self.denses = nn.ModuleList()
        self.denses.append( nn.Linear(lastHiddenDimension,denseDimension) )
        self.denses.append( nn.Linear(denseDimension,denseDimension) )
        
        #causa que mis gradientes exploten
#         self.batchNorms = nn.ModuleList()
#         self.batchNorms.append( nn.BatchNorm1d(denseDimension) )
#         self.batchNorms.append( nn.BatchNorm1d(denseDimension) )
        
        self.dropout = nn.Dropout( dropoutProb )
        
        self.denseFinal = nn.Linear(denseDimension,1)
    
    def forward(self, x, attention_mask):
        x = self.w2v.forward(x, attention_mask).last_hidden_state
        x = self.flatten(x)
        
        for i in range(len(self.denses)):
            x = self.denses[i](x)
            x = F.relu( x )
#             x = self.batchNorms[i](x)
        
        x = self.dropout(x)
            
        return torch.sigmoid( self.denseFinal(x) ) #entre 0 y 1

class Dataset(torch.utils.data.Dataset):
    WAV2VEC2_MAX_INPUT_LENGTH = 3
    
    def __init__(self, dfAudio, dfMetadata = None, kfolds = None, fold = None, foldOut = None, wav2vec2ModelURL = "", augmentation = 0):
        assert (wav2vec2ModelURL != "")
        if dfMetadata is not None:
            assert ( dfMetadata.shape[0] == dfAudio.shape[0] )

        if kfolds != None:
            folds = []
            if foldOut != None:
                folds = [ x for x in range(kfolds) if x != foldOut ]
            else:
                folds = [ fold ]
            
            #me quedo solo con los indices del fold
            self.dfMetaData = dfMetadata[ dfMetadata.kfold.isin(folds) ]
            self.dfAudio = dfAudio[ list(self.dfMetaData.index) ]
            self.dfMetaData.reset_index(drop=True, inplace=True)
        else:
            print("Using the complete dataset (no testing)")
            self.dfMetaData = dfMetadata
            self.dfAudio = dfAudio
        
        self.feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained( wav2vec2ModelURL )
        
        if dfMetadata is not None:
            self.labels = list(self.dfMetaData["labels"])
        else:
            self.labels = None
            print("Not using labels")
        
        if augmentation > 0:
            self.augmentAudio(augmentation)

    def augmentAudio(self, prob):
        # if os.path.exists( f"{FEATURES_PATH}/dfMetadata_aug-{prob}.csv" ):
        #     dfMetadata = pd.read_csv(f"{FEATURES_PATH}/dfMetadata_aug-{prob}.csv")
        #     dfAudio = np.load(f"{FEATURES_PATH}/QOMvsSPA_raw_aug-{prob}.npy", allow_pickle=True)
        #     return dfMetadata, dfAudio

        probProcesses = {
            "Pitch" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.PitchAug( utils.SAMPLE_RATE, zone=(0,1), factor=(-4,4) )
            },
            "Speed" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.SpeedAug( zone=(0,1), factor=(0.5,2) )
            },
            "VTLP" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.VtlpAug( utils.SAMPLE_RATE, coverage=1, zone=(0,1), factor=(0.8,1.2) )
            },
            "Shift" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.ShiftAug( utils.SAMPLE_RATE, duration=0.5 )
            },
        }

        self.dfMetaData.loc[:, "aug_id"] = -1
        self.dfMetaData.loc[:, "aug_process"] = ""

        dfToAug = self.dfMetaData.sample(frac=prob)
        newRows = []
        newAudios = []
        for i, row in tqdm(dfToAug.iterrows(), total=dfToAug.shape[0], desc="Augmenting"):
            for process, config in probProcesses.items():
                for _ in range(config["repeat"]):
                    if np.random.uniform() <= config["prob"]:
                        newAudio = config["aug"].augment( self.dfAudio[ row.name ] )
                        newAudios.append(newAudio)
                            
                        newRow = row.to_dict()
                        newRow["aug_id"] = row.name
                        newRow["aug_process"] = process
                        newRows.append(newRow)

        self.dfMetaData = self.dfMetaData.append(newRows, ignore_index=True)
        self.dfAudio = np.concatenate( [self.dfAudio, newAudios] )

        # np.save( f"{FEATURES_PATH}/QOMvsSPA_raw_aug-{prob}", dfNewAudios )
        # dfNewMetadata.to_csv(f"{FEATURES_PATH}/dfMetadata_aug-{prob}.csv", index=False)

    def collate_wrapper(self, batch):
        
        lstAudio = []
        labels = []
        
        for elem in batch:
            lstAudio.append( elem[0] )
            
            if self.labels is not None:
                labels.append( elem[1] )
            
        pre = self.feature_extractor(
            lstAudio, return_tensors="pt", 
            padding="max_length",
            max_length = utils.SAMPLE_RATE * Dataset.WAV2VEC2_MAX_INPUT_LENGTH,
            truncation = True,
            return_attention_mask=True,
            sampling_rate = utils.SAMPLE_RATE
        )
        
        if self.labels is not None:
            labels = torch.FloatTensor(labels).view(-1,1)
        
            return {
                "input_values": pre.input_values,
                "attention_mask": pre.attention_mask,
                "labels": labels,
            } 
        else:
            return {
                "input_values": pre.input_values,
                "attention_mask": pre.attention_mask
            }
        
    def __len__(self):
#         return len(self.labels)
        return len(self.dfAudio)

    def __getitem__(self, index):
        X = self.dfAudio[index]
        y = 0
        
        if self.labels is not None:
            y = self.labels[index]

        return X, y
    
class DatasetBaseline(torch.utils.data.Dataset):
    WAV2VEC2_MAX_INPUT_LENGTH = 3
    
    def __init__(self, dfAudio, dfMetadata, kfolds, fold = None, foldOut = None, augmentation = 0):
        assert (fold != None or foldOut != None) #xor
        assert ( dfMetadata.shape[0] == dfAudio.shape[0] )

        folds = []
        if foldOut != None:
            folds = [ x for x in range(kfolds) if x != foldOut ]
        else:
            folds = [ fold ]
        
        #me quedo solo con los indices del fold
        self.dfMetaData = dfMetadata[ dfMetadata.kfold.isin(folds) ]
        self.dfAudio = dfAudio[ list(self.dfMetaData.index) ]
        self.dfMetaData.reset_index(drop=True, inplace=True)
        self.labels = list(self.dfMetaData["labels"])

        if augmentation > 0:
            self.augmentAudio(augmentation)

    def augmentAudio(self, prob):
        probProcesses = {
            "Pitch" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.PitchAug( utils.SAMPLE_RATE, zone=(0,1), factor=(-4,4) )
            },
            "Speed" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.SpeedAug( zone=(0,1), factor=(0.5,2) )
            },
            "VTLP" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.VtlpAug( utils.SAMPLE_RATE, coverage=1, zone=(0,1), factor=(0.8,1.2) )
            },
            "Shift" : {
                "repeat" : 1,
                "prob" : 1,
                "aug" : naa.ShiftAug( utils.SAMPLE_RATE, duration=0.5 )
            },
        }

        self.dfMetaData.loc[:, "aug_id"] = -1
        self.dfMetaData.loc[:, "aug_process"] = ""

        dfToAug = self.dfMetaData.sample(frac=prob)
        newRows = []
        newAudios = []
        for i, row in tqdm(dfToAug.iterrows(), total=dfToAug.shape[0], desc="Augmenting"):
            for process, config in probProcesses.items():
                for _ in range(config["repeat"]):
                    if np.random.uniform() <= config["prob"]:
                        newAudio = config["aug"].augment( self.dfAudio[ row.name ] )
                        newAudios.append(newAudio)
                            
                        newRow = row.to_dict()
                        newRow["aug_id"] = row.name
                        newRow["aug_process"] = process
                        newRows.append(newRow)

        self.dfMetaData = self.dfMetaData.append(newRows, ignore_index=True)
        self.dfAudio = np.concatenate( [self.dfAudio, newAudios] )

        # np.save( f"{FEATURES_PATH}/QOMvsSPA_raw_aug-{prob}", dfNewAudios )
        # dfNewMetadata.to_csv(f"{FEATURES_PATH}/dfMetadata_aug-{prob}.csv", index=False)

    def getMFCC(self, data, n_fft = 2048, hop_size = 512, mfcc_components = 20, deltas=True):
        mfcc = librosa.feature.mfcc(data, sr = 16000, n_fft = n_fft, hop_length = hop_size, n_mfcc=mfcc_components)
        #mfcc = np.swapaxes( mfcc, 0, 1 ).copy() #esto no se ve muy performante pero anda bien

        if deltas:
            mfcc_delta_1 = librosa.feature.delta(mfcc, order=1)
            mfcc_delta_2 = librosa.feature.delta(mfcc, order=2)
            mfcc = np.concatenate( (mfcc, mfcc_delta_1, mfcc_delta_2), axis=0 )

        return mfcc
    
    def collate_wrapper(self, batch):
        
        lstAudio = []
        labels = []
        
        for elem in batch:
            #mfcc
            rawAudio = elem[0].copy()
            rawAudio.resize( 16000 * 3 ) #3 segundos

            mfcc = self.getMFCC(rawAudio)
            lstAudio.append(mfcc)
        
            #labels
            labels.append( elem[1] )

        lstAudio = torch.FloatTensor(lstAudio)
        labels = torch.FloatTensor(labels).view(-1,1)
        
        return {
            "input_values": lstAudio,
            "labels": labels,
        }
        
    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        
        X = self.dfAudio[index]
#         X = self.ids[index]
        y = self.labels[index]

        return X, y

    
#debug memory problems
def show_gpu(msg):
    """
    ref: https://discuss.pytorch.org/t/access-gpu-memory-usage-in-pytorch/3192/4
    """
    if not DEBUG_MEMORY:
        return
    
    def query(field):
        return(subprocess.check_output(
            ['nvidia-smi', f'--query-gpu={field}',
                '--format=csv,nounits,noheader'], 
            encoding='utf-8'))
    def to_int(result):
        return int(result.strip().split('\n')[0])
    
    used = to_int(query('memory.used'))
    total = to_int(query('memory.total'))
    pct = used/total
    print(msg, f'{100*pct:2.1f}% ({used}mb out of {total}mb)')

class NoamOpt:
    "Optim wrapper that implements rate."
    def __init__(self, model_size, warmup, optimizer):
        self.optimizer = optimizer
        self._step = 0
        self.warmup = warmup
        self.model_size = model_size
        self._rate = 0
    
    def state_dict(self):
        """Returns the state of the warmup scheduler as a :class:`dict`.
        It contains an entry for every variable in self.__dict__ which
        is not the optimizer.
        """
        return {key: value for key, value in self.__dict__.items() if key != 'optimizer'}
    
    def load_state_dict(self, state_dict):
        """Loads the warmup scheduler's state.
        Arguments:
            state_dict (dict): warmup scheduler state. Should be an object returned
                from a call to :meth:`state_dict`.
        """
        self.__dict__.update(state_dict) 
        
    def step(self):
        "Update parameters and rate"
        self._step += 1
        rate = self.rate()
        for p in self.optimizer.param_groups:
            p['lr'] = rate
        self._rate = rate
        self.optimizer.step()
        
    def rate(self, step = None):
        "Implement `lrate` above"
        if step is None:
            step = self._step
        return (self.model_size ** (-0.5) *
            min(step ** (-0.5), step * self.warmup ** (-1.5))) 