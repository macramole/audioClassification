#!/usr/bin/env python

############
# cantidades que usa voxforge
#############
# dev/for_models     1304 / 20%
# dev/for_probes      300 / 4%
# eval/for_models    1509 / 23%
# eval/for_probes     300 / 4%
# norm/train_world   3148 / 48%
##############
# TOTAL: 6561 / 100%

#%%

import sys
sys.path.append("/media/leandro/stuff/Code/audioClassification")
import dataPaths
import utils
import os
import math
import random
import numpy as np

idiomas = ["qom", "spa"]

#%%

audiosXIdioma = {}

for idioma in idiomas:
    audiosXIdioma[idioma] = []


for path in dataPaths.QOM:
    for idioma in audiosXIdioma:
        finalDir = os.path.join( path, idioma ) + "/"
        if os.path.isdir( finalDir ):
            files = utils.findMusic( finalDir, "wav" )
            audiosXIdioma[idioma] += files

#%%

'''
    dataset : una lista
    splits : una lista con las proporciones de splits que querés ej: [0.1,0.5,0.4] (suma 1)
'''
def splitDataset( dataset, splits ):
    dataset = dataset.copy()
    random.shuffle(dataset)
    
    numberSplits = []
    for split in splits:
        numberSplit = math.floor( split * len(dataset) )
        
        if len(numberSplits) > 0:
            numberSplit += numberSplits[-1]
        numberSplits.append(numberSplit)
    
    sobra = len(dataset) - numberSplits[-1]
    for i in range(0,sobra):
         randomIndex = random.randint(0, len(numberSplits))
         for j in range(randomIndex, len(numberSplits)):
             numberSplits[j] += 1
    
    splitedDataset = []
    for i, iTo in enumerate(numberSplits):
        iFrom = 0
        if i > 0:
            iFrom = numberSplits[i - 1]
        
        splitedDataset.append( dataset[iFrom:iTo] )
        
    print(numberSplits)
    return splitedDataset

archivosProporciones = {
    "dev/for_models" : { "prop": 0.21 },
    "dev/for_probes" : { "prop": 0.04 },
    "eval/for_models" : { "prop": 0.23 }, 
    "eval/for_probes" : { "prop": 0.04 },
    "norm/train_world" : { "prop": 0.48 }
}

for idioma in idiomas:
    splited = splitDataset( audiosXIdioma[idioma], [ x["prop"] for x in archivosProporciones.values() ])
    for i, a in enumerate(archivosProporciones.values()):
        a[idioma] = splited[i]


