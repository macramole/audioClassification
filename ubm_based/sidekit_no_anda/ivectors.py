#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 11:27:33 2018

@author: leandro
"""

#%%

import utils
import sidekit
import pandas as pd
from glob import glob

LABELS = ["spa","qom"]

PATH_AUDIO = "/home/leandro/Data/QOM_Tiago/slices_processed/"
PATH_AUDIO_ALL = "/home/leandro/Data/QOM_Tiago/slices_processed/all/"
PATH_OUTPUT = "/home/leandro/Data/QOM_Tiago/features/"

#%% Data collection

audioFiles = utils.findMusic( PATH_AUDIO, "wav" ) 
durations = utils.getDurations( audioFiles )
df = pd.DataFrame({"path" : audioFiles, "duration" : durations})

#df[ df.duration < 0.3 ].shape[0] / df.shape[0] 
df = df[ df.duration >= 0.3 ]

audioFiles = {"spa":None,"qom":None}
audioFiles["spa"] = list ( df[ df.path.str.find("spa") != -1 ][ "path" ] )
audioFiles["qom"] = list ( df[ df.path.str.find("qom") != -1 ][ "path" ] )

#%%

#import shutil
#
#for f in audioFiles["qom"]:
#    show = f[ f.rfind("/")+1:f.rfind(".") ]
#    show = "qom_" + show
#    shutil.copy(f, PATH_AUDIO + "all/" + show + ".wav")
#    
#for f in audioFiles["spa"]:
#    show = f[ f.rfind("/")+1:f.rfind(".") ]
#    show = "spa_" + show
#    shutil.copy(f, PATH_AUDIO + "all/" + show + ".wav")
    
#%% Feature Extraction
# Mejorar parámetros (son sólo de prueba)

#featuresExtractor = sidekit.FeaturesExtractor( 
#        window_size=0.1, 
#        shift=0.1,
#        sampling_frequency=44100,
#        filter_bank="log",
#        filter_bank_size=10,
#        ceps_number=20,
##        vad="energy",
#        save_param=["cep"],
#        lower_frequency=1000, 
#        higher_frequency=10000 )
#
#for label in LABELS:
#    for audioFile in audioFiles[label]:
#        show = audioFile[ audioFile.rfind("/")+1:audioFile.rfind(".") ]
#        show = label + "_" + show
#        
#        print(show)
#        
#        featuresExtractor.save(show, 
#                               input_audio_filename = audioFile, 
#                               output_feature_filename=PATH_OUTPUT+show+".hdf5")

#%% Train a Universal Background Model
        
featuresExtractor = sidekit.FeaturesExtractor( 
        audio_filename_structure = "{}",
        window_size=0.1, 
        shift=0.1,
        sampling_frequency=44100,
        filter_bank="log",
        filter_bank_size=10,
        ceps_number=20,
#        vad="energy",
        save_param=["cep"],
        lower_frequency=1000, 
        higher_frequency=10000 )

featuresServer = sidekit.FeaturesServer( features_extractor = featuresExtractor,
                                         dataset_list = ["cep"],
                                         )
       
ubm = sidekit.Mixture()

ubm.EM_split(features_server= featuresServer,
#             feature_list= glob(PATH_OUTPUT + "*.hdf5"),
             feature_list= list(df["path"]),
             distrib_nb=64,
             iterations=(1, 2, 2, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8),
             num_thread=1,
             save_partial=False,
             ceil_cov=10,
             floor_cov=1e-2
             )

#%%

#import h5py
#
#feats = []
#for feat in glob(PATH_OUTPUT + "*.hdf5"):
#    feats.append( h5py.File(feat, "r") )
#
#list(f.keys())
#list( f["qom_10763_11761"].keys() )
#f["qom_10763_11761"]["cep_mean"].shape
