#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 15:19:24 2019

@author: leandro
"""

import Dataset
import os

target = "/home/leandro/Data/QOM/all/{}/{}"
idiomas =  [ "spa", "qom", "und" ]
filenames, labels = Dataset.getFilesAndLabels( idiomas )

for i in range(len(labels)):
    label = idiomas[ labels[i] ]
    filename = filenames[i]

    os.symlink( filename, target.format(label, os.path.basename(filename)) )

