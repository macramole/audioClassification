#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 15:07:12 2018

@author: leandro
"""

#%% Imports, consts, global vars

#import os
#import librosa
import numpy as np
import time
import Dataset

from keras.models import Sequential
from keras.layers import Masking, LSTM
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam

from sklearn.model_selection import train_test_split

from ResultLogger import ResultLogger
import Metrics

logger = ResultLogger("results/results.lstm.variable.qom.new.csv")

#%% Dataset 

DATASET_NAME, dfAudio, labels = Dataset.getQOMvsSPA_lstm()

#DATASET_NAME, dfAudio, labels = Dataset.getUNDvsQOMandSPA_lstm()
    
#%% Train

NOTES = ""
BATCH_SIZE = 512

seeds = [ 86143, 93763, 67967 ]
#seeds = [ 86143 ]

dfSplit = []
for i in range(0, len(seeds)):
    X_train, X_test, y_train, y_test = train_test_split(dfAudio, labels, random_state = seeds[i])
    
#    y_train = to_categorical( y_train )
#    y_test = to_categorical( y_test )
    y_train = np.array( y_train, dtype=int )
    y_test = np.array( y_test, dtype=int )
    
    dfSplit.append( {
        "X_train" : X_train,
        "X_test" : X_test,
        "y_train" : y_train,
        "y_test" : y_test 
    } )


#epochsStep = 25
#epochsMax = 600
epochsStep = 1
epochsMax = 2

# lrs = [0.001, 0.0001, 0.00001]
# dropouts = [0.2,0.4,0.6]
# unitss = [ 5, 10, 20, 50, 100, 200 ]
# layerss = [1,2,3,4]

lrs = [0.001]
dropouts = [0]
unitss = [ 10 ]
layerss = [1]

for layers in layerss:
    for units in unitss:
        for lr in lrs:
            for dropout in dropouts:
                currentRun = {
                    "# layers" : layers,
                    "units" : units,
                    "dropout" : dropout,
                    "lr" : lr
                }
                
                print("Current run:")
                print(currentRun, "\n\n")
                
                if logger.isRunAlreadyMade( currentRun ):
                    print("skip\n")
                    continue
                
                metrics = []
                for idxSplit in range(len(dfSplit)):
                    
                    print("\n Training seed",idxSplit,"\n")
                    
                    split = dfSplit[idxSplit]
                    X_train, X_test, y_train, y_test = split["X_train"],split["X_test"],split["y_train"],split["y_test"]
                    
                    ###############################
                    ############ MODEL ############
                    ###############################
                    
                    model = Sequential()
                    model.add(Masking(mask_value=0., input_shape=(dfAudio.shape[1], dfAudio.shape[2])))
                    
                    for l in range(layers):
                        model.add(LSTM(units, dropout=dropout, return_sequences= (l!=layers-1) ))
                    
                    model.add(Dense(1))
                    model.add(Activation('sigmoid'))
                    
                    adam = Adam(lr=lr)
                    model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])
                    model.summary()
                       
                    ##############################
                    ##############################
                    
                    metrics.append([])
                    for i in range(int(epochsMax/epochsStep)):
                        tic = time.time()
                        model.fit( X_train, y_train, epochs = epochsStep, batch_size = BATCH_SIZE, validation_data=(X_test, y_test) )
                        toc = time.time()
                        
                        tiempo = toc - tic #(en segundos)
                        
                        metric = Metrics.getMetrics( model, X_train, y_train, X_test, y_test, tiempo )
                        metrics[idxSplit].append( metric )
                        
                        print("Metric:\n", metric)
                        
                    
                averageMetrics = Metrics.getAverageMetrics(metrics)
                currentEpochs = 0
                for i in range(int(epochsMax/epochsStep)):
                    logger.setValue("dataset", DATASET_NAME)
                    logger.setValue("notes", NOTES)
                    
                    logger.setValue("lr", lr)
                    logger.setValue("units", units)
                    logger.setValue("# layers", layers)
                    logger.setValue("dropout", dropout)
                    logger.setValue("currentEpochs", currentEpochs)
                    logger.setValue("tiempoPromedio", averageMetrics["time"][i])
                    
                    logger.saveMetrics(metrics, averageMetrics, i)
                    logger.log()
                    
                    currentEpochs += epochsStep