#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 15:07:12 2018

@author: leandro
"""

#%% Imports, consts, global vars

#import os
#import librosa
import numpy as np
import time
import Dataset

from keras.models import Sequential
from keras.layers import Input
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, GlobalMaxPooling2D
from keras.optimizers import Adam

from sklearn.model_selection import train_test_split

from ResultLogger import ResultLogger
import Metrics

logger = ResultLogger("results/qom_vs_spa.conv.csv")

#%% Dataset

# Uso seteo de "Using Deep Neural Networks for Identification of Slavic Languages from Acoustic Signal"

#sampleRate = 22050 #es el definido en utils
#n_fft = sampleRate*25//1000 #25ms
#hop_size = sampleRate*10//1000 #10ms
#mfcc_components = 13

DATASET_NAME, df, labels = Dataset.getQOMvsSPA_conv( max_length=3.0 ) # + del 80% de los datos tienen menos de 3s

#%% Train

NOTES = "max3s" #"Win:25ms.Hop:10ms.Comp:13"
seeds = [ 86143, 93763, 67967 ]
#seeds = [ 86143 ]

dfSplit = []
for i in range(0, len(seeds)):
    X_train, X_test, y_train, y_test = train_test_split(df, labels, random_state = seeds[i])
    
    y_train = np.array( y_train, dtype=int )
    y_test = np.array( y_test, dtype=int )
    
    dfSplit.append( {
        "X_train" : X_train,
        "X_test" : X_test,
        "y_train" : y_train,
        "y_test" : y_test 
    } )

layerss = [ 1, 2, 3 ]
fclayerss = [ [], [50], [50, 50], [50, 20], [100], [100, 100], [100, 50] ]
unitss = [ 8, 16, 32, 64, 128, 256  ]
lrs = [0.0001, 0.00001]
dropouts = [0, 0.1, 0.2]
batchsizes = [128]

epochsStep = 20
epochsMax = 500
#epochsStep = 1
#epochsMax = 2

for layers in layerss:
    for fclayers in fclayerss:
        for units in unitss:
            for lr in lrs:
                for dropout in dropouts:
                    for batchsize in batchsizes:
                        modelParams = {
                            "notes" : NOTES,
                            "# layers" : layers,
                            "# FC layers" : fclayers,
                            "units" : units,
                            "dropout" : dropout,
                            "lr" : lr,
                            "batchSize" : batchsize,
			    "currentEpochs" : epochsMax-epochsStep #esto es solo para que chequee bien isRunAlreadyMade
                        }
                        
                        print("Current run:")
                        print(modelParams, "\n\n")
                        
                        if logger.isRunAlreadyMade( modelParams ):
                            print("skip\n")
                            continue
                        
                        metrics = []
                        for idxSplit in range(len(dfSplit)):
                            
                            print("\n Training seed",idxSplit,"\n")
                            
                            split = dfSplit[idxSplit]
                            X_train, X_test, y_train, y_test = split["X_train"],split["X_test"],split["y_train"],split["y_test"]
                            
                            ###############################
                            ############ MODEL ############
                            ###############################
                            
                            model = Sequential()
                            
                            ############ CONV LAYERS ############
                            for l in range(layers):
                                if l == 0:
                                    model.add( Conv2D( units // layers, (7,7), input_shape= df.shape[1:], activation="relu", padding="same" ) )
                                else:
        #                            model.add( Conv2D( int(units/( 2 ** l ) ) , (3, 3), activation="relu", padding="same" ) )
                                    model.add( Conv2D( units // (layers-l) , (3, 3), activation="relu", padding="same" ) )
                                    
                                if dropout > 0:
                                    model.add(Dropout(dropout))
                                
                                if l < layers - 1:
                                    #a veces no se puede poolear mas porque la dimensión quedó 1
                                    try:
                                        model.add(MaxPooling2D(pool_size=(2, 2)))
                                    except:
                                        pass
                            
                            ############ DENSE LAYERS ############
                            model.add(GlobalMaxPooling2D())
                            
                            for fcsUnits in fclayers:
                                model.add(Dense(fcsUnits))
                                                        
                            model.add(Dense(1))
                            model.add(Activation('sigmoid'))
                            
                            adam = Adam(lr=lr)
                            model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])
                            model.summary()
                    
                            ##############################
                            ##############################
                            
                            metrics.append([])
                            for i in range(int(epochsMax/epochsStep)):
                                tic = time.time()
                                model.fit( X_train, y_train, epochs = epochsStep, batch_size = batchsize, validation_data=(X_test, y_test) )
                                toc = time.time()
                                
                                tiempo = toc - tic #(en segundos)
                                
                                metric = Metrics.getMetrics( model, X_train, y_train, X_test, y_test, tiempo )
                                metrics[idxSplit].append( metric )
                                
                                print("Metric:\n", metric)
                                
                        averageMetrics = Metrics.getAverageMetrics(metrics)
                        currentEpochs = 0
                        for i in range(int(epochsMax/epochsStep)):
                            logger.setValue("dataset", DATASET_NAME)
                                                   
                            for m in modelParams:
                                logger.setValue(m, modelParams[m])
                                                    
                            logger.setValue("currentEpochs", currentEpochs+epochsStep)
                            logger.setValue("tiempoPromedio", averageMetrics["time"][i])
                            
                            logger.saveMetrics(metrics, averageMetrics, i)
                            logger.log()
                            
                            currentEpochs += epochsStep
