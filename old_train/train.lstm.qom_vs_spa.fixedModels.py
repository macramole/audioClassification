#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 15:07:12 2018

@author: leandro
"""

#%% Imports, consts, global vars

#import os
#import librosa
import numpy as np
import time
import Dataset

from keras.models import Sequential
from keras.layers import Masking, LSTM, BatchNormalization
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam

from sklearn.model_selection import train_test_split

from ResultLogger import ResultLogger
import Metrics

logger = ResultLogger("results/qom_vs_spa.lstm.csv")

#%% Dataset

# Uso seteo de "Using Deep Neural Networks for Identification of Slavic Languages from Acoustic Signal"

#sampleRate = 22050 #es el definido en utils
#n_fft = sampleRate*25//1000 #25ms
#hop_size = sampleRate*10//1000 #10ms
#mfcc_components = 13

DATASET_NAME, df, labels = Dataset.getQOMvsSPA_lstm()

#%% Train

NOTES = "newdata8-10" #"Win:25ms.Hop:10ms.Comp:13"
seeds = [ 86143, 93763, 67967 ]
#seeds = [ 86143 ]

dfSplit = []
for i in range(0, len(seeds)):
    X_train, X_test, y_train, y_test = train_test_split(df, labels, random_state = seeds[i])
    
    y_train = np.array( y_train, dtype=int )
    y_test = np.array( y_test, dtype=int )
    
    dfSplit.append( {
        "X_train" : X_train,
        "X_test" : X_test,
        "y_train" : y_train,
        "y_test" : y_test 
    } )


models = [
    {
        "# LSTM layers" : 2,
        "# FC layers" : [100],
        "units" : 100,
        "dropout" : 0.15,
        "lr" : 0.0001,
        "batchSize" : 128
    },
    {
        "# LSTM layers" : 2,
        "# FC layers" : [100, 50],
        "units" : 100,
        "dropout" : 0.1,
        "lr" : 0.0001,
        "batchSize" : 128
    },
    {
        "# LSTM layers" : 2,
        "# FC layers" : [100, 100],
        "units" : 100,
        "dropout" : 0.15,
        "lr" : 0.0001,
        "batchSize" : 128
    },
    {
        "# LSTM layers" : 2,
        "# FC layers" : [50, 50],
        "units" : 100,
        "dropout" : 0.15,
        "lr" : 0.0001,
        "batchSize" : 128
    },
    {
        "# LSTM layers" : 1,
        "# FC layers" : [],
        "units" : 200,
        "dropout" : 0.2,
        "lr" : 0.001,
        "batchSize" : 256
    },
]

epochsStep = 50
epochsMax = 500
#epochsStep = 1
#epochsMax = 2

for model in models:
    layers = model["# LSTM layers"]
    fclayers = model["# FC layers"]
    units = model["units"]
    dropout = model["dropout"]
    lr = model["lr"]
    batchsize = model["batchSize"]
    model["notes"] = NOTES #asi lo chequea en isRunAlreadyMade
    
    print("Current run:")
    print(model, "\n\n")
    
    if logger.isRunAlreadyMade( model ):
        print("skip\n")
        continue
    
    metrics = []
    for idxSplit in range(len(dfSplit)):
        
        print("\n Training seed",idxSplit,"\n")
        
        split = dfSplit[idxSplit]
        X_train, X_test, y_train, y_test = split["X_train"],split["X_test"],split["y_train"],split["y_test"]
        
        ###############################
        ############ MODEL ############
        ###############################
        
        model = Sequential()
        model.add(Masking(mask_value=0., input_shape=(df.shape[1], df.shape[2])))
        
        for l in range(layers):
            model.add(LSTM(units, dropout=dropout, return_sequences= (l!=layers-1) ))
        
        for fcsUnits in fclayers:
            model.add(Dense(fcsUnits))
            #model.add(BatchNormalization())
        
        model.add(Dense(1))
        model.add(Activation('sigmoid'))
        
        adam = Adam(lr=lr)
        model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])
        model.summary()

        ##############################
        ##############################
        
        metrics.append([])
        for i in range(int(epochsMax/epochsStep)):
            tic = time.time()
            model.fit( X_train, y_train, epochs = epochsStep, batch_size = batchsize, validation_data=(X_test, y_test) )
            toc = time.time()
            
            tiempo = toc - tic #(en segundos)
            
            metric = Metrics.getMetrics( model, X_train, y_train, X_test, y_test, tiempo )
            metrics[idxSplit].append( metric )
            
            print("Metric:\n", metric)
            
        
    averageMetrics = Metrics.getAverageMetrics(metrics)
    currentEpochs = 0
    for i in range(int(epochsMax/epochsStep)):
        logger.setValue("dataset", DATASET_NAME)
        logger.setValue("notes", NOTES)
        
        logger.setValue("batchSize", batchsize)
        logger.setValue("lr", lr)
        logger.setValue("units", units)
        logger.setValue("# LSTM layers", layers)
        logger.setValue("# FC layers", fclayers)
        logger.setValue("dropout", dropout)
        logger.setValue("currentEpochs", currentEpochs)
        logger.setValue("tiempoPromedio", averageMetrics["time"][i])
        
        logger.saveMetrics(metrics, averageMetrics, i)
        logger.log()
        
        currentEpochs += epochsStep
