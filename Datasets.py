#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 17:22:00 2019

@author: leandro
"""
import dataPaths
import os
import utils
import numpy as np
import pandas as pd

from scipy.io.wavfile import write as writeAudio
from sklearn.model_selection import StratifiedKFold

from transformers import Wav2Vec2FeatureExtractor

import nlpaug.augmenter.audio as naa

DATASET_MFCC = "MFCC"
DATASET_RAW = "raw"
DATASET_OPENSMILE = "openSMILE"
DATASET_WAV2VEC2 = "wav2vec2"

WAV2VEC2_MODEL_URL = "facebook/wav2vec2-large-100k-voxpopuli"
WAV2VEC2_MAX_INPUT_LENGTH = 3 #seconds

FEATURES_PATH = "./features"
SEGMENTOS_PARA_RESCATAR_PATH = "../segmentosParaRescatar.csv"

def getMetadata( idiomas = [ "spa", "qom" ] ):
    metadataFilename = f"{FEATURES_PATH}/dfMetadata.csv"
    
    if not os.path.isfile( metadataFilename ):
        dfFileNames, labels = getFilesAndLabels(idiomas)

        speakers = []
        speakersAgg = []

        for f in dfFileNames:
            start = (f.rfind("/")+1)
            end = start + 3
            speaker = f[start:end]
            speakers.append(speaker)

            if speaker[0:2] == "CH" or speaker in ["AUY"]:
                speakersAgg.append("Niñe")
            elif speaker[0:2] == "AD":
                if "Tiziana" in f:
                    if speaker[2] == "1":
                        speakersAgg.append("Hombre")
                    elif speaker[2] == "3":
                        speakersAgg.append("Mujer")
                    else:
                        print(f"Warning: {speaker} es inesperado para Tiziana")
                        speakersAgg.append("Adulte")
                else:
                    speakersAgg.append("Adulte")

            elif speaker[0:2] == "HO" or speaker in ["PAP","FAC"]:
                speakersAgg.append("Hombre")
            elif speaker[0] == "M" or speaker in ["TIA", "ABA", "NIL", "BRE"]:
                speakersAgg.append("Mujer")
            elif speaker == "LOR":
                speakersAgg.append("Extra")
            else:
                print(f"Warning: {speaker} not aggregable")
                speakersAgg.append("Extra")

        durations = utils.getDurations(dfFileNames)

        lang = []
        for l in labels:
            lang.append( idiomas[l] )

        dfMetadata = pd.DataFrame({"filenames" : dfFileNames, "speakers" : speakers, "speakers_agg" : speakersAgg, "durations" : durations, "labels" : labels, "lang" : lang})
        return cleanMetadata(dfMetadata)
    else :
        return pd.read_csv(metadataFilename)

def cleanMetadata(df):
    #Borro los extras (loro por ejemplo)
    idxExtra = df.speakers_agg == "Extra"
    df = df[~idxExtra]
    
    #Rescato algunos segmentos <= 1.2 segundos (en general son basura)
    dfRescate = pd.read_csv(SEGMENTOS_PARA_RESCATAR_PATH)
    df.loc[:,"basenames"] = df.apply(lambda L: os.path.basename(L.filenames), axis=1)
    df = df[ (df["durations"] > 1.2) | (df["basenames"].isin(dfRescate.file)) ].drop( columns="basenames" )
    
    df.reset_index(inplace=True, drop = True)
    return df

def cutLongAudios(dfMetadata, dfAudio):
    MAX_INPUT_LENGTH = 3
    output = "/home/leandro/Data/QOM/largosCortados"
    
    if ( dfMetadata[ dfMetadata.durations >= MAX_INPUT_LENGTH + 2 ].shape[0] == 0 ):
        return dfMetadata, dfAudio
    
    newRows = []
    newAudios = []

    for i, row in dfMetadata[ dfMetadata.durations >= MAX_INPUT_LENGTH + 2 ].iterrows():
        newAudiosTmp = []
        audio = dfAudio[i].copy()

        last = 0
        gapSize = MAX_INPUT_LENGTH * utils.SAMPLE_RATE
        for j in range(gapSize, audio.size, gapSize):
            newAudiosTmp.append( audio[last:j] )
            last = j
        
        if audio[last:].size >= 2 * utils.SAMPLE_RATE:
            newAudiosTmp.append( audio[last:] )
        else:
            newAudiosTmp[-1] = np.concatenate( (newAudiosTmp[-1], audio[last:]) )

        for j, audio in enumerate(newAudiosTmp):
            newAudios.append(audio)

            sujeto = row.filenames.split("/")[5]

            scaled = np.int16(audio/np.max(np.abs(audio)) * 32767)
            filename = f'{i}-{j}-' + os.path.basename(row["filenames"])
            filename = f'{output}/{sujeto}/{filename}'
            os.system(f'mkdir "{output}/{sujeto}/" -p')
            writeAudio(filename, utils.SAMPLE_RATE, scaled)

            newRow = row.to_dict()
            newRow["filenames"] = filename
            newRow["durations"] = scaled.size / utils.SAMPLE_RATE
            newRows.append(newRow)
    
    idxConservar = dfMetadata.durations < MAX_INPUT_LENGTH + 2
    dfMetadata = dfMetadata[ idxConservar ]
    dfAudio = dfAudio[ idxConservar ]
    dfMetadata.reset_index(inplace=True, drop = True)

    dfMetadata = dfMetadata.append(newRows, ignore_index=True)
    dfAudio = np.concatenate((dfAudio, newAudios))
    
    dfMetadata.to_csv(f"{FEATURES_PATH}/dfMetadata.csv", index=False)
    np.save( f"{FEATURES_PATH}/QOMvsSPA_raw", dfAudio )
    
    return dfMetadata, dfAudio

def getFilesAndLabels( idiomas = [ "spa", "qom" ] ):
    dfFileNames = []
    labels = []
    
    for path in dataPaths.QOM:
        for labelIdioma, idioma in enumerate(idiomas):
            finalDir = os.path.join( path, idioma ) + "/"
            if os.path.isdir( finalDir ):
                files = utils.findMusic( finalDir, "wav" )
                            
                for f in files:
                    dfFileNames.append(f)
                    labels.append(labelIdioma)
                    
    
    return (dfFileNames, labels)

def getQOMvsSPA( dataset = DATASET_MFCC, 
                    split = "speaker", 
                    n_fft = 2048, 
                    hop_size = 512, 
                    mfcc_components = 20, 
                    max_length = -1, 
                    seedSplit = 34132 ):

    datasetName = f"QOMvsSPA_{dataset}_{split}"
    dfAudio = None
    
    dfMetadata = getMetadata( idiomas=[ "spa", "qom" ] )
        
    if dataset == DATASET_MFCC:
        
        # Esto no quedó bien realmente, cuando lo usé dfMetadata ya venia cortado (cutLongAudios)
        # Si inicio el proceso todo de nuevo (por ejemplo borrando la carpeta features/)
        # debería pedir primero RAW que anda bien y después sí ya puedo pedir MFCC
        
        npyFilename = f"{FEATURES_PATH}/QOMvsSPA_mfcc_{n_fft}_{hop_size}_{mfcc_components}_{max_length}.npy"
        if not os.path.isfile( npyFilename ):
            print("Dataset file not found. Building...")
            if max_length == -1:
                utils.setSizeAudioFromLargestFile(list(dfMetadata.filenames))
            else :
                utils.setSegundosFila(max_length)

            dfAudio = utils.getAudioData( list(dfMetadata.filenames), features = "mfcc", superVector = False, n_fft = n_fft, hop_size = hop_size, mfcc_components = mfcc_components, deltas=True )
            np.save( npyFilename[0:-4], dfAudio )
            print("Done")
        else:
            dfAudio = np.load(npyFilename)
    
    elif dataset == DATASET_RAW:
        npyFilename = f"{FEATURES_PATH}/QOMvsSPA_raw.npy"
        if not os.path.isfile( npyFilename ):
            print("Dataset file not found. Building...")
            
            dfAudio = utils.getAudioData( list(dfMetadata.filenames), features = "raw" )
            np.save( npyFilename[0:-4], dfAudio )
        else:
            dfAudio = np.load(npyFilename, allow_pickle=True)
    
    elif dataset == DATASET_WAV2VEC2:
        _, dfRaw, _ = getQOMvsSPA( DATASET_RAW )

        lstAudio = []
        for i, audio in enumerate(dfRaw):
            lstAudio.append( audio )

        feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained( WAV2VEC2_MODEL_URL )
        dfAudio = feature_extractor(
            lstAudio, return_tensors="pt", 
            padding=True,
            max_length = utils.SAMPLE_RATE * WAV2VEC2_MAX_INPUT_LENGTH,
            do_normalize=True, 
            return_attention_mask=True,
            sampling_rate = utils.SAMPLE_RATE)
    
    elif dataset == DATASET_OPENSMILE:
        npyFilename = f"{FEATURES_PATH}/QOMvsSPA_openSmile_features.npy"
        if not os.path.isfile( npyFilename ):
            import opensmile
            
            smile = opensmile.Smile(
                feature_set=opensmile.FeatureSet.ComParE_2016,
                feature_level=opensmile.FeatureLevel.Functionals,
            )
            
            files, labels = Datasets.getFilesAndLabels(["spa","qom"])
            y = smile.process_files(files)
            np.save(npyFilename[0:-4], y.to_numpy())
        else:
            dfAudio = np.load(npyFilename)       
    
    ##########
    # Ya tengo el dfAudio. Si no se cortó que se corte
    ##########
    
    dfMetadata, dfAudio = cutLongAudios(dfMetadata, dfAudio)
    
    ##########
    # Folds
    ##########
    
    if split == "estratificado":
        # creo un solo split teniendo en cuenta label, speaker y duración
        dfMetadata["durationsDiscreto"] = pd.qcut(dfMetadata.durations, 3)
        dfMetadata["labelsDurationsSpeakerAgg"] = dfMetadata["labels"].astype(str) + "-" + dfMetadata["speakers_agg"].astype(str) + "-" + dfMetadata["durationsDiscreto"].astype(str)
        
        #tengo muy pocos 0-Hombre-(1.695, 13.94] asi que los voy a juntar con 0-Hombre-(0.93, 1.695]
        dfMetadata.loc[dfMetadata.labelsDurationsSpeakerAgg == "0-Hombre-(1.695, 13.94]", "labelsDurationsSpeakerAgg"] = "0-Hombre-(0.93, 1.695]"
        
#         skf = StratifiedKFold(n_splits=5, random_state=seedSplit, shuffle = False) #shuffle true estaria bien, no se si es problema
        skf = StratifiedKFold(n_splits=5)
        dfMetadata["kfold"] = -1
        k = 0
        for train_index, test_index in skf.split(list(dfMetadata.index), dfMetadata.labelsDurationsSpeakerAgg):
            dfMetadata.loc[ test_index, "kfold"] = k
            k += 1
    elif split == "speaker":
        # Cada fold tiene (casi) speakers independientes de los demás. foldsCalculate.ods tiene la data de cómo los hice
        dfMetadata["sujeto"] = dfMetadata.filenames.str.split("/", expand=True)[5]
        dfMetadata.loc[dfMetadata.sujeto == "largosCortados", "sujeto"] = dfMetadata[dfMetadata.sujeto == "largosCortados"].filenames.str.split("/", expand=True)[6]
        dfMetadata["sujeto-speakers-language"] = dfMetadata["sujeto"] + "-" + dfMetadata["speakers"] + "-" + dfMetadata["lang"]
        
        dfMetadata["kfold"] = -1

        dfMetadata.loc[
            (dfMetadata["sujeto"] == "Abelardo") |
            (dfMetadata["sujeto"] == "Tiago") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-HO1-qom"),
            "kfold"
        ] = 0

        dfMetadata.loc[
            (dfMetadata["sujeto"] == "Kiara") |
            (dfMetadata["sujeto"] == "Tiziana") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU3-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU3-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH2-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-ABA-qom"),
            "kfold"
        ] = 1

        dfMetadata.loc[
            (dfMetadata["sujeto"] == "Ayuyu") |
            (dfMetadata["sujeto"] == "Emanuel") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MAM-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MAM-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH2-qom"),
            "kfold"
        ] = 2

        dfMetadata.loc[
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH1-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH1-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH3-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CH3-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CHI-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-HO2-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-HO2-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU2-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU2-spa"),
            "kfold"
        ] = 3

        dfMetadata.loc[
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-CHI-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-HO1-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-HO3-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU1-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-MU1-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-PAP-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-PAP-spa") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-TIA-qom") |
            (dfMetadata["sujeto-speakers-language"] == "Nahuel-TIA-spa"),
            "kfold"
        ] = 4
    elif split == "family":
        dfMetadata["sujeto"] = dfMetadata.filenames.str.split("/", expand=True)[5]
        dfMetadata.loc[dfMetadata.sujeto == "largosCortados", "sujeto"] = dfMetadata[dfMetadata.sujeto == "largosCortados"].filenames.str.split("/", expand=True)[6]
        dfMetadata["kfold"] = dfMetadata["sujeto"].astype("category").cat.codes
    
    return (datasetName, dfAudio, dfMetadata)

def getQOMvsSPA_lstm( n_fft = 2048, hop_size = 512, mfcc_components = 20, max_length = -1 ):
    datasetName = "QOMvsSPA_LSTM"
    dfAudio = None
    
    dfFileNames, labels = getFilesAndLabels( idiomas=[ "spa", "qom" ] )
    
    npyFilename = "QOMvsSPA_mfcc_{}_{}_{}_{}.npy".format(n_fft, hop_size, mfcc_components, max_length)
    dfAudio = None
    if not os.path.isfile( npyFilename ):
        if max_length == -1:
            utils.setSizeAudioFromLargestFile(dfFileNames)
        else :
            utils.setSegundosFila(max_length)
            
        dfAudio = utils.getAudioData( dfFileNames, features = "mfcc", superVector = False, n_fft = n_fft, hop_size = hop_size, mfcc_components = mfcc_components )
        np.save( npyFilename[0:-4], dfAudio )
    else:
        dfAudio = np.load(npyFilename)
    
    return (datasetName, dfAudio, labels)

def getQOMvsSPA_conv( n_fft = 2048, hop_size = 512, mfcc_components = 20, max_length = -1 ):
    datasetName, dfAudio, labels = getQOMvsSPA_lstm( n_fft, hop_size, mfcc_components, max_length )
    datasetName = "QOMvsSPA_CONV"
    
    return (datasetName, np.expand_dims(dfAudio, 3), labels)

def getUNDvsQOMandSPA_lstm( n_fft = 2048, hop_size = 512, mfcc_components = 20, max_length = -1 ):
    datasetName = "UNDvsQOMandSPA_lstm"
    dfAudio = None
    
    dfFileNames, labels = getFilesAndLabels()
    
    npyFilename = "UNDvsALL_mfcc_variable_{}_{}_{}_{}.npy".format(n_fft, hop_size, mfcc_components, max_length)
    if not os.path.isfile( npyFilename ):
        if max_length == -1:
            utils.setSizeAudioFromLargestFile(dfFileNames)
        else :
            utils.setSegundosFila(max_length)
            
        dfAudio = utils.getAudioData( dfFileNames, features = "mfcc", superVector = False, n_fft = n_fft, hop_size = hop_size, mfcc_components = mfcc_components )
        np.save( npyFilename[0:-4], dfAudio )
    else:
        dfAudio = np.load(npyFilename)
    
    labels = np.array(labels)
    labels[ labels == 1 ] = 0
    labels[ labels == 2 ] = 1
    
    return (datasetName, dfAudio, labels)

def getUNDvsQOMandSPA_lstm_BUT():
    datasetName = "UNDvsQOMandSPA_lstm_BUT"
    
    filenames, labels = getFilesAndLabels()
    filenamesBUT = np.load("BUTFeatures/QOM_features_210819_filenames.npy")
    features = np.load("BUTFeatures/QOM_features_210819.npy")
    
    labelsBUT = []

    for f in filenamesBUT:
        f = f.decode('UTF-8')
        if f in filenames:
            labelsBUT.append( labels[filenames.index(f)] )
    
    
    labels = np.array(labelsBUT)
    labels[ labels == 1 ] = 0
    labels[ labels == 2 ] = 1
    
    return (datasetName, features, labels)
    
def getUNDvsQOMandSPA_lstm__MFCC_BUT():
    datasetName = "UNDvsQOMandSPA_lstm__MFCC_BUT"
    
    filenames, labels = getFilesAndLabels()
    filenamesBUT = np.load("BUTFeatures/QOM_features_210819_filenames.npy")
    
    _, features, labelsBUT = getUNDvsQOMandSPA_lstm_BUT()
    _, mfcc, labels = getUNDvsQOMandSPA_lstm()
    
    filteredMFCC = []
    toPad = features.shape[1] - mfcc.shape[1]
    
    for f in filenamesBUT:
        f = f.decode('UTF-8')
        i = filenames.index(f)
        
        filteredMFCC.append( np.pad(mfcc[i], [(0,toPad),(0,0)], "constant") )
    
    filteredMFCC = np.array(filteredMFCC)
    
    features = np.concatenate( (features, filteredMFCC), axis=2 )
    
    return (datasetName, features, labelsBUT)