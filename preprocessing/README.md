# ¿Cómo hacer el preprocessing?

Este es un documento para Leandro del futuro sobre cómo preprocesar los audios antes de intentar clasificar para el sistema de LID.

## Si lo tenemos en CHA

Necesitamos:
1. Un archivo CHA bulleteado
2. El archivo WAV relacionado

### 1. Recortar por bullets

Para eso usamos scripts/splitAudio.py

### 2. Usamos To Combo SAD (speech activity detector)

1. Copiamos el resultado al directorio de la DiViMe.
2. vagrant up ( a veces está un rato con el cannot connect )
3. vagrant ssh
4. ~/To-Combo-SAD/runBatch.sh <directorio>. Si estamos adentro de la VM, el directorio /vagrant/ apunta al mismo directorio de la DiViMe. Entonces por ejemplo: *~/To-Combo-SAD/runBatch.sh /vagrant/miData/spa*. Hay que hacer esto para spa y qom
5. exit para irnos de la VM

### 3. Recortar por RTTM

6. Usamos scripts/splitAudioByRTTM.py

## Si lo tenemos en EAF

Necesitamos:
1. Un archivo EAF
2. El archivo WAV relacionado

### 1. Recortar por regiones

Usamos scripts/splitAudioByEAF.py
