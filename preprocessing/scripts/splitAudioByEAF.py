#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 14:13:15 2018

@author: leandro
"""

import argparse
import sys
import os

# sys.path.insert(0, '/home/leandro/Code/ciipme/chaParser')
sys.path.insert(0, '/home/macramole/Code/ciipme/chaParser')
from EafFile import *

parser = argparse.ArgumentParser()
parser.add_argument("pathEafFile", help="Path to EAF file")
parser.add_argument("pathWavFile", help="Path to WAV file")

args = parser.parse_args()

eaf = EafFile(args.pathEafFile)
eaf.segmentAudio(args.pathWavFile)
