#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: leandro
"""

import argparse
from glob import glob
from pydub import AudioSegment

parser = argparse.ArgumentParser()
parser.add_argument("pathRTTM", help="Path to RTTM files (with names: tocombosad*.rttm)")
parser.add_argument("pathOutput", help="Where do you want the results?")
args = parser.parse_args()

FIELD_FILENAME = 1
FIELD_TIME_FROM = 3
FIELD_TIME_LENGTH = 4

for filePath in glob( "%s/tocombosad*.rttm" % args.pathRTTM ):
    with open( filePath ) as f:
        #go to last row
        for row in f:
            pass

        arrRow = row.split("\t")
        filename = arrRow[FIELD_FILENAME].strip()
        timeFrom = float(arrRow[FIELD_TIME_FROM])*1000
        timeLength = float(arrRow[FIELD_TIME_LENGTH])*1000
        timeTo = timeFrom + timeLength

        wavFile = AudioSegment.from_wav("%s/%s.wav" % (args.pathRTTM,filename))
        wavFile = wavFile[ timeFrom:timeTo ]

        wavFile.export("%s/%s.wav" % (args.pathOutput,filename), format="wav")

       # print( filename, timeFrom, timeLength )
