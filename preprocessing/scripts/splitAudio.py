#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 14:13:15 2018

@author: leandro
"""

import argparse
import sys
import os

sys.path.insert(0, '/home/leandro/Code/ciipme/chaParser')
import chaParser
chaParser.CLAN_BIN_PATH = "/home/leandro/apps/unix-clan/bin/"

parser = argparse.ArgumentParser()
parser.add_argument("pathChaFile", help="Path to CHA file")
parser.add_argument("pathWavFile", help="Path to WAV file")
parser.add_argument("pathOutput", help="Where do you want the results?")

args = parser.parse_args()

# Verificar que existe ./slices/spa ./slices/qom ./slices/und
#chaParser.splitAudioByBullets("./tiagogutiérrez1ro290315.cha","./tiagogutiérrez1ro290315.wav")

if not os.path.isdir(args.pathOutput):
    print("ERROR: Output path don't exist.")
    exit()
elif os.path.isdir("%s/%s" % (args.pathOutput, "slices")):
    print("ERROR: Slices dir already exist inside output dir")
    exit()
else:
    os.mkdir("%s/%s" % (args.pathOutput, "slices/"))
    os.mkdir("%s/%s" % (args.pathOutput, "slices/spa/"))
    os.mkdir("%s/%s" % (args.pathOutput, "slices/und/"))
    os.mkdir("%s/%s" % (args.pathOutput, "slices/qom/"))


chaParser.splitAudioByBullets(args.pathChaFile,args.pathWavFile,args.pathOutput)
