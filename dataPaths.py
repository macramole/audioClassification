#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 13:58:34 2018

@author: leandro
"""

# Todos estos directorios tienen dos carpetas "qom y spa".
# Algunos tienen "und" y algunos pocos tienen "unk"

QOM = [
        # bullets a partir de CHA. falta bulletear
        "/home/leandro/Data/QOM/Tiago/1era Toma/slices_processed/",

        # hay segmentado sólo habla de chiques
        "/home/leandro/Data/QOM/Tiago/3era Toma/slices",

        # hay segmentado sólo habla de chiques
        "/home/leandro/Data/QOM/Tiziana/3era toma/slices/",

        # bullets a partir de CHA. falta bulletear
        "/home/leandro/Data/QOM/Tiziana/1era toma/slices_result/",

    	# Primera hora del audio
        "/home/leandro/Data/QOM/Nahuel/2da Toma/slices", #agregue 14feb 2020
       	# Primera hora del audio
    	"/home/leandro/Data/QOM/Nahuel/3era Toma/slices",

       	# Primera hora del audio
        "/home/leandro/Data/QOM/Abelardo/2da Toma/slices", #agregue 14feb 2020
        # Tres horas sólo chicos (no se si porque sólo hay chicos o porque si fue una decisión)
        "/home/leandro/Data/QOM/Abelardo/3era Toma/slices",

        # De hora 4 a hora 5, incluye loro
        "/home/leandro/Data/QOM/Tiziana/2da toma/slices/", #agregue 8oct 2020. este dia tambien se actualizaron los que ya estaban
        # De hora 5 a 6
        "/home/leandro/Data/QOM/Tiago/2da Toma/slices", #agregue 8oct 2020

		# "/home/leandro/Data/QOM/Nahuel/1era Toma/slices", #agregué 24 de febrero 2021, primer highVol, la corrección hizo temis, habia dudas que Ana tenía que mirar
        "/home/leandro/Data/QOM/Nahuel/1era Toma/Nahuel-T1_slices", #el anterior incluia las dudas, lo cual es incorrecto

        #agregué 2 de agosto 2021 los siguientes son todos highVol, hay dudas para Ana
        "/home/leandro/Data/QOM/Kiara/Kiara-T1_slices",
        "/home/leandro/Data/QOM/Ayuyu/Ayuyu-T1-1_slices",
        "/home/leandro/Data/QOM/Ayuyu/Ayuyu-T1-2_slices",
        "/home/leandro/Data/QOM/Emanuel/Emanuel-T1-1_slices",
        "/home/leandro/Data/QOM/Emanuel/Emanuel-T1-3_slices",

        #agrego 17 de noviembre 2021,
        #es el audio segmentado por Temis a partir de transcripcion
        "/home/leandro/Data/QOM/Abelardo/1era Toma/slices",
        #es el audio segmentado por Nahuel a partir de transcripcion
        "/home/leandro/Data/QOM/Nahuel/1era Toma/nahuel-segmentado-desde-transcripcion"

]
QOM_test = [
        "/data/QOM/abelardo_slices",
        "/data/QOM/nahuel_slices",
]
